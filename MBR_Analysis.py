import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from nptdms import TdmsFile

filename = "000000_MABR02A_Anfang_0636UMin_0125Hz_035V_08ms"
begin = 360 # first datapoint
min_crop_voltage = 0.4 # minimal voltage for crop rms

input_filename = filename + ".tdms"
output_filename_rms = filename + "_rms" + ".csv"
output_filename_mmax = filename + "_Mmax" + ".csv"
output_filename_crop_rms = filename + "_crop_rms" + ".csv"
tdms_file = TdmsFile.read(input_filename) # file name
rpm = int(input_filename[22:26]) # rotations per minute
mag_freq = int(input_filename[31:35]) # magentization frequency in Hz
sample_freq = 500000 # sample frequency in Hz
formfactor = 1.11 # form factor for RMS calculation
turn = 0

# Load TDMS data
def load_tdms():
    tdms_groups = tdms_file.groups()
    select_group = str(tdms_groups[0])

    char1 = "/"
    char2 = ">"
    select_group = select_group[select_group.find(char1)+2 : select_group.find(char2)+1]

    group = tdms_file[select_group]
    channel = group['Hysterese0']
    channel_data = channel[:]
    # plt.plot(range(len(channel_data)), channel_data)
    # plt.show()
    return channel_data

# Number of full Samples per 1 Turn
def samples_num(n_mag_full, sample_freq, mag_freq):
    n_samples = n_mag_full*(sample_freq/mag_freq)
    return int(n_samples)

# Number of magnetization per 1 Turn
def mag_num(rpm, mag_freq):
    n_mag = mag_freq/(rpm/60)
    return n_mag

# Number of full magnetization per 1 Turn
def mag_num_full(n_mag):
    return int(n_mag)

# Difference between turns an samplings
def calc_factor(n_mag_full, n_mag):
    factor = n_mag_full/n_mag
    return factor

# Calculation RMS
def calc_rms(channel_data, chunk_size, formfactor):
    channel_data = np.array(channel_data) * 50
    channel_data = channel_data**2
    channel_data = np.sqrt(np.sum(channel_data, axis=1)/chunk_size)
    channel_data = channel_data * formfactor
    return channel_data

# Reducing channel_data for 1 Turn
def reduce_data(channel_data, n_samples, begin):
    end = begin + n_samples
    channel_data = channel_data[begin:end]
    return channel_data

# Split channel_data
def split_list(n_samples, n_mag_full, channel_data):
    chunk_size = int(n_samples/n_mag_full)
    channel_data = np.array(channel_data).reshape(-1, chunk_size)
    return (channel_data, chunk_size)

# Calculation Mmax
def calc_mmax(channel_data):
    channel_data = np.abs(channel_data)
    channel_data = np.max(channel_data, axis=1) * 50
    return channel_data

# Crop data
def crop_channel_data(min_crop_voltage, data):
    return [row[np.abs(row)>min_crop_voltage] for row in data]

# Calc RMS out of cropped data
def calc_rms_list(data, formfactor):
    return [calc_rms(row.reshape(1,-1), len(row), formfactor)[0] for row in data]
    
channel_data = load_tdms() # load data
n_mag = mag_num(rpm, mag_freq) # number of magnetizations
n_mag_full = mag_num_full(n_mag) # number of magnetizations for 1 turn
factor = calc_factor(n_mag_full, n_mag) # factor between magnetizations and magnetizations for 1 turn
n_samples = samples_num(n_mag_full, sample_freq, mag_freq) # number of samples
channel_data = reduce_data(channel_data, n_samples, begin) # keep required data only
channel_data, chunk_size = split_list(n_samples, n_mag_full, channel_data) # split data for each magnetization
rms = calc_rms(channel_data, chunk_size, formfactor) # calculate rms
Mmax = calc_mmax(channel_data) # calculate Mmax
channel_data = crop_channel_data(min_crop_voltage, channel_data) # filter data for crop rms
crop_rms = calc_rms_list(channel_data, formfactor) # calculate crop rms

winkel = np.arange(0, 360, 360 / n_mag_full) + turn
rad = winkel / 180 * np.pi

result = pd.DataFrame({
    "winkel" : winkel,
    "rms": rms
})
result.index += 1
result.to_csv(output_filename_rms, index=False, header=False, sep="\t",)

result1 = pd.DataFrame({
    "winkel" : winkel,
    "Mmax": Mmax
})
result1.index += 1
result1.to_csv(output_filename_mmax, index=False, header=False, sep="\t",)

result2 = pd.DataFrame({
    "winkel" : winkel,
    "crop_rms": crop_rms
})
result2.index += 1
result2.to_csv(output_filename_crop_rms, index=False, header=False, sep="\t",)

#  print(n_mag_full)
fig = plt.figure()
ax = fig.add_subplot(polar=True)
# print(crop_rms)
ax.set_ylim(0,500)
ax.plot(rad, rms, "-", color='black', label='RMS')
# ax.plot(rad, Mmax, "-", color='cyan', label='Mmax')
# ax.plot(rad, crop_rms, "-", color='red', label='crop RMS')
ax.legend(loc='upper center')
plt.show()