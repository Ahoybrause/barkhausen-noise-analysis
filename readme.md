## About the Python script
This script is used for data processing of recorded measurement data of the Stresstech Rollscan 350.
For this purpose, the data stored with the help of a DAQ card and a LabView program as `.tdms` are required.

With the help of the script the Barkhausen noise is determined analog to the software ViewScan.
The intention is to achieve a higher data quality and to determine the RMS of each magnetization.

The program is used for the analysis of recorded measurement data of a rotating sample.

## Python and packages
The script is based on [Python](https://www.python.org/) and requires a running Python3 installation.

In addition, the following packages must be installed:
`numpy`, `pandas`, `nptdms`

## Procedure and operation
### How to use
Add the `filename` with the same length and the same order of the parameters.

Adjust the filename if necessary or change the detection of the variables out of the filename.

## Variables
The following variables are used:

|Variable|Declaration|
|-----|-----|
|`filename`|Filename|
|`sample_freq`|Sampling rate|
|`rpm`|Rotations per Minute|
|`mag_freq`|Magnetization frequency|
|`channel_data`|All recorded data out of the data logging channel|
|`n_mag`|Number of magnetizations for one turn|
|`n_mag_full`|Completed number of magnetizations for one turn|
|`factor`|Factor between `n_mag` and `n_mag_full`|
|`n_samples`|Number of measurements for the rounded number of magnetizations|
|`chunk_size`|Number of measurements per magnetization|
|`rms`|Root Mean Square for each magnetization|

## Output file
The output file contains the RMS of each magnetization during the first rotation of the sample with an index in the first column.